document.querySelector('.tabs').addEventListener('click', (event) => {
	if (event.target.classList.contains('tabs-title')) { // Проверили, что кликнули на tabs-title

		// Находим tabs, у которого сейчас class="active" и удаляем этот класс,
		document.querySelector('.tabs .active').classList.remove('active');

		// а тому tab-у, по которму кликнули добавляем class="active"
		event.target.classList.add('active');

		// Удаляем class="active" контенту у которого он есть
		document.querySelector('.tabs-content .active').classList.remove('active');

		// Добавляем class="active" контенту с таким же атрибутом
		document.querySelector(
			`.tabs-content li[data-tab="${event.target.dataset.tab}"]`
		).classList.add('active')
	}
})
